import React from 'react'

import Item from './Item'
import './ItemList.scss'

const ItemList = props => {
    const items = props.list.map((item) => {
        return <li className="item"><Item item={item}/></li>
    })

    return (
        <ul className="item-list">{items}</ul>
    )
}

export default ItemList;