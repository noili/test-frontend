import React, { Component } from 'react'

import './Item.scss'

class Item extends Component {
    render() {
        return (
            <div className="row justify-content-center item-row">
                <div className="img-wrapper col col-2">
                    <a href={`/items/${this.props.item.id}`}>
                        <img src={this.props.item.picture} className="thumbnail" height="180" width="180" alt=""/>
                    </a>
                </div>
                <div className="details-wrapper col col-8">
                    <div className="pl-wrapper row">
                        <p className="price col col-9">
                            $ {this.props.item.price.amount}
                        </p>
                        <p className="location col col-3">
                            {this.props.item.province}
                        </p>
                    </div>
                    <a href={`/items/${this.props.item.id}`}>
                        <p className="description">
                            {this.props.item.title}
                        </p>
                    </a>
                </div>
            </div>
        )
    }
}

export default Item;