import React, { Component } from 'react'

import './Details.scss'

class Details extends Component {
    state = {
        detail: {
            author: {
                name: '',
                lastName: ''
            },
            item: {
                id: '',
                title: '',
                price: {
                    currency: '',
                    amount: 0,
                    decimals: 0
                },
                picture: '',
                condition: '',
                free_shipping: false,
                sold_quantity: 0,
                description: ''
            }
        }
    }

    componentDidMount() {
        fetch(`http://localhost:8080/items/${this.props.match.params.id}`)
        .then(results => {
            return results.json()
        }).then(data => {
            this.setState({detail: data})
        });
    }

    render() {
        return (
            <div>
                <div className="details something">
                    <div className="row justify-content-center picture-and-details">
                        <div className="picture-wrapper col col-7">
                            <img src={this.state.detail.item.picture} className="thumbnail" height="auto" width="680" alt="" />
                        </div>
                        <div className="details col col-3">
                            <p className="status">
                                {this.state.detail.item.condition} {this.state.detail.item.sold_quantity ? `-${this.state.detail.item.sold_quantity}` : ''}
                            </p>
                            <p className="title">
                                {this.state.detail.item.title}
                            </p>
                            <p className="price">
                                $ {this.state.detail.item.price.amount}
                            </p>
                            <button type="" className="btn btn-primary btn-block buy-button">
                                Comprar
                            </button>
                        </div>
                    </div>
                    <div className="description-wrapper row justify-content-center">
                        <h3 className="title col col-10">
                            Descripción del producto
                        </h3>
                        <p className="description col col-10">
                            {this.state.detail.item.description}
                        </p>
                    </div>
                </div>
            </div>
        )
    }
}

export default Details;