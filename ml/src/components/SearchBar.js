import React, { Component } from 'react';
import './SearchBar.scss';
import ic_Search from '../assets/icons/ic_Search.png';
import Logo_ML from '../assets/icons/Logo_ML.png';

class SearchBar extends Component {
    state = {value: ''}

    render() {
        return (
            <div>
                <nav className="search-bar row justify-content-center">
                    <div className="logo col col-1">
                        <img src={Logo_ML} className="search-icon"  alt=""/>
                    </div>
                    <form className="input-group mb-3 col col-9 input-search" action={`/items?search=${this.state.value}`} method="get">
                        <input 
                            type="text"
                            name="search"
                            className="form-control"
                            placeholder="Nunca dejes de buscar"
                            value={this.state.value}
                            onChange={e => this.setState({value: e.target.value})}
                        ></input>
                        <div className="input-group-append">
                            <button type="submit" className="btn btn-secondary input-group-text">
                                <img src={ic_Search} className="search-icon" alt=""/>
                            </button>
                        </div>
                    </form>
                </nav>
            </div>
        )
    }
}

export default SearchBar;