import React, { Component } from 'react'

import './Breadcrumb.scss'

class Breadcrumb extends Component {
    render() {
        return (
            <div className="breadcrumb">
                <p className="col col-10 crumbs-wrapper">
                    {this.props.crumbs.join(' < ')}
                </p>
            </div>
        )
    }
}

export default Breadcrumb;