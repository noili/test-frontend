import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from "react-router-dom";

import SearchBar from './components/SearchBar'
import ItemList from './components/ItemList'
import Details from './components/Details'
import Breadcrumb from './components/Breadcrumb';

class App extends Component {
  state = {
    items: [],
    q: '',
    filters: []
  }

  handleSubmit = q => {
    fetch(`http://localhost:8080/items?q=${q}&limit=4`)
    .then(results => {
        return results.json()
    }).then(data => {
      this.setState({
        items: data.items,
        filters: data.filters[0].path_from_root.map((element) => {return element.name})
      })
    });
  }

  getParameterByName(name, url) {
    if (!url) url = window.location.href
    name = name.replace(/[\[\]]/g, '\\$&')
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'), results = regex.exec(url)
    if (!results) return null; if (!results[2]) return ''
    return decodeURIComponent(results[2].replace(/\+/g, ' '))
  }

  componentDidMount() {
    this.handleSubmit(this.getParameterByName('search'))
  }

  render() {
    return (
      <div className="App">
        <SearchBar />
        {this.state.filters.length > 0 ? <Breadcrumb crumbs={this.state.filters}/> : ''}
        <Router>
          <div>
            <Route
              path="/items"
              render={(props) => <ItemList {...props} list={this.state.items}/>}
            />
            <Route path="/items/:id" component={Details} />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
