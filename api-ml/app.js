const express = require('express')
var cors = require('cors');
const app = express()
const port = 8080
const fetch = require('node-fetch')
const baseURL = 'https://api.mercadolibre.com'

app.use(cors());

app.get('/', (req, res) => res.send('Hello World!'))

app.get('/items', ({query}, res) => search(query, res))

const search = ({q, limit}, res) => {
    fetch(`${baseURL}/sites/MLA/search?q=${q}&limit=${limit}`)
    .then(results => {
        return results.json()
    }).then(data => {
        return res.send({
            author: createAuthor(),
            categories: createCategories(data.results),
            items: createItemList(data.results),
            filters: data.filters[0].values
        })
    });
}

const createItemList = (items) => {
    return items.map(createProduct)
}

const createPrice = (item) => {
    return {
        currency: item.currency_id,
        amount: item.price,
        decimals: howManyDecimals(item.price)
    }
}

const howManyDecimals = (n) => {
    if (Math.floor(n) === n) return 0
    return n.toString().split('.')[1].length || 0
}

const createAuthor = () => {
    return {
        name: 'Cosme',
        lastname: 'Fulanito'
    }
}

const createProduct = (item) => {
    return {
        id: item.id,
        title: item.title,
        price: createPrice(item),
        picture: item.thumbnail,
        condition: item.condition,
        free_shipping: item.shipping.free_shipping,
        province: item.address.state_name
    }
}

const createCategories = (items) => {
    return items.map((element) => {
        return element.category_id
    })
}

app.get('/items/:id', ({params}, res) => details(params, res))

const details = ({id}, res) => {
    fetch(`${baseURL}/items/${id}`)
    .then(results => {
        return results.json()
    }).then(data => {
        return description(id, res, data)
    });
}

const description = (id, res, item) => {
    fetch(`${baseURL}/items/${id}/description`)
    .then(results => {
        return results.json()
    }).then(detail => {
        return res.send({
            author: createAuthor(),
            item: createDetailedProduct(item, detail)
        })
    });
}

const createDetailedProduct = (item, detail) => {
    return {
        id: item.id,
        title: item.title,
        price: createPrice(item),
        picture: item.pictures[0].url,
        condition: item.condition,
        free_shipping: item.shipping.free_shipping,
        sold_quantity: item.sold_quantity,
        description: detail.plain_text
    }
}

app.listen(port, () => console.log(`Example app listening on port ${port}!`))