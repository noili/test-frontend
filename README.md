# Test frontend

## Clone repository

`git clone https://gitlab.com/noili/test-frontend.git`

### Backend

Open console at /api-ml

`npm install`

`node app.js`

it mounts at :8080

### Frontend

Open console at /ml

`npm install`

`npm start`

it mounts at :3000